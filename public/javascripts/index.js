function processSortMethods() {
  $( "#content" ).html("");
  $.get( "sorts/array", function( data ) {
    var i = 0;
    var htmlDiv = [""];
    htmlDiv[i++] = "<h3> Array:</h3>";
    htmlDiv[i++] = "<div>";
    htmlDiv[i++] = generarLineas(data, 1000, ",");
    htmlDiv[i++] = "</div>";
    $( "#array" ).html(htmlDiv.join(""));
    callSortMethod("selection");
    callSortMethod("bubble");
    callSortMethod("heap");
    callSortMethod("shell");
    callSortMethod("quick");
  });
  function generarLineas(array, maxElement, union) {
    var cadena = [""];
    for (var i=0; i < array.length; i++) {
      cadena[i] = array[i] + union;
      var mod = (i + 1 )% maxElement;
      if (mod==0) {
        cadena[i] = cadena[i] + "<br>";
      }
    }
    return cadena.join("");
  }
  function callSortMethod(method) {
    $.ajax( {
      "url": "sorts/" + method,
      success: function( data ) {
        var i = 0;
        var htmlDiv = [""];
        htmlDiv[i++] = "<h3> Metodo: ";
        htmlDiv[i++] = method;
        htmlDiv[i++] = " - Duracion: ";
        htmlDiv[i++] = data.timeDuration;
        htmlDiv[i++] = "ms </h3>";
        htmlDiv[i++] = "<div>";
        htmlDiv[i++] = generarLineas(data.list, 1000, ",");
        htmlDiv[i++] = "</div>";
        $( "#" + method + "Sort").html(htmlDiv.join(""));

      }
    });
  }
}
