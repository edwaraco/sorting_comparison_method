name := """ordination_method_comparison"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)
resolvers += "Jackson Repository" at "https://repository-master.mulesoft.org/nexus/content/groups/public/"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
 "org.apache.commons" % "commons-lang3" % "3.4",
 "org.codehaus.jackson" % "jackson-core-asl" % "1.9.13",
 "org.codehaus.jackson" % "jackson-mapper-asl" % "1.9.13"
)

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator
