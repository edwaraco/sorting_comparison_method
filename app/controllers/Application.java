package controllers;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import play.*;
import play.libs.Json;
import play.mvc.*;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import play.mvc.BodyParser;
import models.*;
import views.html.*;
import utils.IntegerArrayRandomBuilder;

public class Application extends Controller {
    private static final List<Integer> intList;
    static {
      intList = IntegerArrayRandomBuilder.generarArreglo();
    }

    private class ResponseSort implements Serializable {
      private String timeDuration;
      private List<Integer> list;

      public String getTimeDuration() {
        return timeDuration;
      }

      public List<Integer> getList() {
        return list;
      }
    }

    public Result index() {
      return ok(index.render("Matriz que se debe ordenar: "));
    }


    public Result getArray() {
      try {
        //List<Integer> arrayToOrder = IntegerArrayRandomBuilder.generarArreglo();
        return ok(Json.toJson(intList));
      } catch (Exception e) {
        return internalServerError();
      }
    }

    public Result sortShellMethod() {
      try {
        ResponseSort result = invokeSortArrayMethod(ShellSort.class);
        return ok(Json.toJson(result));
      } catch (Exception e) {
        return internalServerError();
      }
    }

    public Result sortHeapMethod() {
      try {
        ResponseSort result = invokeSortArrayMethod(HeapSort.class);
        return ok(Json.toJson(result));
      } catch (Exception e) {
        return internalServerError();
      }
    }

    public Result sortQuickMethod() {
      try {
        ResponseSort result = invokeSortArrayMethod(QuickSort.class);
        return ok(Json.toJson(result));
      } catch (Exception e) {
        return internalServerError();
      }
    }

    public Result sortSelectionMethod() {
      try {
        ResponseSort result = invokeSortArrayMethod(SelectionSort.class);
        return ok(Json.toJson(result));
      } catch (Exception e) {
        return internalServerError();
      }
    }

    public Result sortBubbleMethod() {
      try {
        ResponseSort result = invokeSortArrayMethod(BubbleSort.class);
        return ok(Json.toJson(result));
      } catch (Exception e) {
        return internalServerError();
      }
    }

    private <T extends SortMethod> ResponseSort invokeSortArrayMethod(Class<T> clazz) throws Exception {
      List<Integer> arrayToOrder = intList;
      SortMethod sorterObject = SortMethodBuilder.createInstance(clazz);
      Integer[] arrayOrder = sorterObject.sortArray(arrayToOrder.toArray(new Integer[0]));
      ResponseSort responseSort = new ResponseSort();
      responseSort.timeDuration = sorterObject.getTimeDuration();
      responseSort.list = Arrays.asList(arrayOrder);
      return responseSort;
    }
}
