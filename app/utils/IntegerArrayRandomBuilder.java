package utils;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class IntegerArrayRandomBuilder {
  private static Integer maxLimit = 10000;
  private static final int NTHREDS = 100;
  private static class MyCallable implements Callable<Integer> {
    private Random rand;
    public MyCallable(Random rand) {
      this.rand = rand;
    }
    @Override
    public Integer call() throws Exception {

      return (int) (1 + rand.nextInt() * 100) * maxLimit;
    }
  }
  private static List<Future<Integer>> crearListaFuturos(ExecutorService executor) {
    List<Future<Integer>> list = new ArrayList<Future<Integer>>();
    Random rand = new Random();
    for (int i = 0; i < maxLimit; i++) {
      Callable<Integer> worker = new MyCallable(rand);
      Future<Integer> submit = executor.submit(worker);
      list.add(submit);
    }
    return list;
  }

  public static List<Integer> generarArreglo() {
    ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
    List<Future<Integer>> listFuture = crearListaFuturos(executor);
    int tamMax = listFuture.size();
    List<Integer> arrayToOrd = new ArrayList<Integer>();
    for (int i = 0; i < tamMax; i++) {
      Future<Integer> future = listFuture.get(i);
      try {
        arrayToOrd.add(future.get());
      } catch (InterruptedException e) {
        System.out.println("Error...");
        arrayToOrd.add(i);
      } catch (ExecutionException e) {
        System.out.println("Error...");
        arrayToOrd.add(i);
      }
    }
    executor.shutdown();
    return arrayToOrd;
  }
}
