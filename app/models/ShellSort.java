package models;

public class ShellSort extends SortMethod {

  /**
  * Mientras se intercambie algún elemento
  * se da una pasada
  * y si están desordenados
  * se reordenan
  * y se marca como cambio.
  * @see http://puntocomnoesunlenguaje.blogspot.com.co/2014/09/metodo-shell-de-ordenacion.html
  */
  protected Integer[] sort(Integer[] array) {
    int salto, aux, i;
    int sizeArray = array.length;
    Integer[] arrayOrdered = array;
    boolean cambios;
    for(salto = sizeArray/2; salto!=0; salto/=2) {
      cambios=true;
      while(cambios) {
        cambios=false;
        for(i=salto; i < sizeArray; i++) {
          int diff = i-salto;
          if(arrayOrdered[diff] > arrayOrdered[i]) {
            aux = arrayOrdered[i];
            arrayOrdered[i] = arrayOrdered[diff];
            arrayOrdered[diff]=aux;
            cambios=true;
          }
        }
      }
    }
    return arrayOrdered;
  }
}
