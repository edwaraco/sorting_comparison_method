package models;
/*
*  @See http://java2novice.com/java-sorting-algorithms/quick-sort/#sthash.2cUkc66H.dpuf
*/
public class QuickSort extends SortMethod {


   private int length;

   public Integer[] sort(Integer[] inputArr) {
       if (inputArr == null || inputArr.length == 0) {
           return null;
       }
       Integer[] array = inputArr;
       length = inputArr.length;
       quickSort(array, 0, length - 1);
       return array;
   }

   private void quickSort(Integer[] array, int lowerIndex, int higherIndex) {

       int i = lowerIndex;
       int j = higherIndex;
       // calculate pivot number, I am taking pivot as middle index number
       int pivot = array[lowerIndex+(higherIndex-lowerIndex)/2];
       // Divide into two arrays
       while (i <= j) {
           /**
            * In each iteration, we will identify a number from left side which
            * is greater then the pivot value, and also we will identify a number
            * from right side which is less then the pivot value. Once the search
            * is done, then we exchange both numbers.
            */
           while (array[i] < pivot) {
               i++;
           }
           while (array[j] > pivot) {
               j--;
           }
           if (i <= j) {
               exchangeNumbers(array, i, j);
               //move index to next position on both sides
               i++;
               j--;
           }
       }
       // call quickSort() method recursively
       if (lowerIndex < j)
           quickSort(array, lowerIndex, j);
       if (i < higherIndex)
           quickSort(array, i, higherIndex);
   }

   private void exchangeNumbers(Integer[] array, int i, int j) {
       int temp = array[i];
       array[i] = array[j];
       array[j] = temp;
   }
}
