package models;

/**
* @see http://www.programmingsimplified.com/java/source-code/java-program-to-bubble-sort
*/
public class BubbleSort extends SortMethod {
  public Integer[] sort(Integer[] inputArray) {
    Integer[] array = inputArray;
    int swap;
    int n = array.length;
    for (int c = 0; c < ( n - 1 ); c++) {
      for (int d = 0; d < n - c - 1; d++) {
        if (array[d] > array[d+1]) {
          swap       = array[d];
          array[d]   = array[d+1];
          array[d+1] = swap;
        }
      }
    }
    return array;
  }
}
