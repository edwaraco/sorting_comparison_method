package models;

/**
* http://java2novice.com/java-sorting-algorithms/selection-sort/#sthash.KO2Zhbki.dpuf
*/
public class SelectionSort extends SortMethod {

  public Integer[] sort(Integer[] inputArray) {
    Integer[] arr = inputArray;
    for (int i = 0; i < arr.length - 1; i++) {
      int index = i;
      for (int j = i + 1; j < arr.length; j++) {
        if (arr[j] < arr[index]) {
          index = j;
        }
      }
      int smallerNumber = arr[index];
      arr[index] = arr[i];
      arr[i] = smallerNumber;
    }
    return arr;
  }
}
