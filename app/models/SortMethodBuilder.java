package models;

public class SortMethodBuilder {
  private SortMethodBuilder() {
  }

  public static <T extends SortMethod> T createInstance(Class<T> clazz) throws InstantiationException, IllegalAccessException {
    return clazz.newInstance();
  }
}
