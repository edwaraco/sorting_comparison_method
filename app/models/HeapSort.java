package models;

/**
* @see http://www.sanfoundry.com/java-program-implement-heap-sort/
*/
public class HeapSort extends SortMethod {
    private static int tamArr;
    /* Sort Function */
    public Integer[] sort(Integer[] arr) {
        Integer[] arraySort = arr;
        heapify(arraySort);
        for (int i = tamArr; i > 0; i--) {
            swap(arraySort,0, i);
            tamArr = tamArr - 1;
            maxheap(arraySort, 0);
        }
        return arraySort;
    }
    /* Function to build a heap */
    public static void heapify(Integer[] arr) {
        tamArr = arr.length-1;
        for (int i = tamArr/2; i >= 0; i--) {
          maxheap(arr, i);
        }
    }
    /* Function to swap largest element in heap */
    public static void maxheap(Integer[] arr, int i) {
        int left = 2*i ;
        int right = 2*i + 1;
        int max = i;
        if (left <= tamArr && arr[left] > arr[i]) {
          max = left;
        }
        if (right <= tamArr && arr[right] > arr[max]) {
          max = right;
        }
        if (max != i) {
            swap(arr, i, max);
            maxheap(arr, max);
        }
    }
    /* Function to swap two numbers in an array */
    public static void swap(Integer[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}
