package models;

public abstract class SortMethod {
  private String timeDuration;

  protected abstract Integer[] sort(Integer[] array);

  public Integer[] sortArray(Integer[] array) {
    long start = System.currentTimeMillis();
    Integer[] arrayOrdered = sort(array);
    timeDuration = String.valueOf(System.currentTimeMillis() - start);
    return arrayOrdered;
  }

  public String getTimeDuration() {
    return timeDuration;
  }
}
